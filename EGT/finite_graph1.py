import math
import scipy.special
import numpy as np
import matplotlib.pyplot as plt


def generate_dove_payoffs(N):
    """ Initializes the payoffs array for doves in all configurations of k and N """
    dove_payoffs = []
    for k in range(0, N + 1):
        dove_payoffs.append(dove_payoff(N, k))
    return dove_payoffs


def generate_hawk_payoffs(N, fight_cost):
    """ Initializes the payoffs array for hawks in all configurations of k and N """
    hawk_payoffs = []
    for k in range(0, N + 1):
        hawk_payoffs.append(hawk_payoff(N, k, fight_cost))
    return hawk_payoffs


def dove_payoff(N, k):
    """ returns the payoff for a dove in a sample of size N containing k doves """
    nb_hawk = N - k
    R = 1  # R (reward) is assumed to be 1
    if nb_hawk == 0:
        return R / N
    else:
        return 0


def hawk_payoff(N, k, Ch):
    """ returns the payoff for a hawk in a sample of size N containing k doves """
    nb_hawk = N - k
    if nb_hawk == 0:
        return 0
    R = 1  # R (reward) is assumed to be 1
    value = (R - ((nb_hawk - 1) * Ch)) / nb_hawk
    return value


def averageFitnessDove(k, Z, N, dove_payoffs):
    """ returns the average fitness of all the doves in the population of size Z with k doves and sample size N """
    sumAvg = []
    for i in range(N):
        val1 = scipy.special.binom(k - 1, i)
        val2 = scipy.special.binom(Z - k, N - i - 1)
        denom = scipy.special.binom(Z - 1, N - 1)
        avg = (val1 * val2) / denom
        sumAvg.append(avg * dove_payoffs[i + 1])
    return np.sum(sumAvg)


def averageFitnessHawk(k, Z, N, hawk_payoffs):
    """ returns the average fitness of all the hawks in the population of size Z with k doves and sample size N """
    sumAvg = []
    for i in range(N):
        val1 = scipy.special.binom(k, i)
        val2 = scipy.special.binom(Z - k - 1, N - i - 1)
        denom = scipy.special.binom(Z - 1, N - 1)
        avg = (val1 * val2) / denom
        sumAvg.append(avg * hawk_payoffs[i])
    return np.sum(sumAvg)


def probDoveChangeToHawk(b, k, Z, N):
    """ Fermi-like rule: returns the probability that a dove changes to a hawk """
    avgFitnessDoves = averageFitnessDove(k, Z, N)
    avgFitnessHawks = averageFitnessHawk(k, Z, N)
    return 1. / (1. + np.exp(-b * (avgFitnessDoves - avgFitnessHawks)))


def probHawkChangeToDove(b, k, Z, N):
    """ Fermi-like rule: returns the probability that a hawk changes to a dove """
    avgFitnessDoves = averageFitnessDove(k, Z, N)
    avgFitnessHawks = averageFitnessHawk(k, Z, N)
    return 1. / (1. + np.exp(-b * (avgFitnessHawks - avgFitnessDoves)))


def probPopGainOneDove(k, Z, N, b):
    first = k / Z
    second = (Z - k) / Z
    fitnessDiff = averageFitnessDove(k, Z, N) - averageFitnessHawk(k, Z, N)
    third = 1.0 / 1 + np.exp(-b * fitnessDiff)
    return first * second * third


def probPopLoseOneDove(k, Z, N, b):
    first = k / Z
    second = (Z - k) / Z
    fitnessDiff = averageFitnessDove(k, Z, N) - averageFitnessHawk(k, Z, N)
    third = 1.0 / 1 + np.exp(+b * fitnessDiff)
    return first * second * third


def gradientOfSelection(k, Z, b, N, dove_payoffs, hawk_payoffs):
    first = k / Z
    second = (Z - k) / Z
    fitnessDiff = averageFitnessDove(k, Z, N, dove_payoffs) - averageFitnessHawk(k, Z, N, hawk_payoffs)
    third = b / 2 * fitnessDiff
    return first * second * math.tanh(third)



fight_costs = [0.1, 0.5]
population_sizes = [10, 20, 100]
N = 5
b = 1  # equal to w apparently

gradients = []
for popsizeId in range(len(population_sizes)):
    gradients.append([])
    Z = population_sizes[popsizeId]
    for costId in range(len(fight_costs)):
        cost = fight_costs[costId]
        dove_payoffs = generate_dove_payoffs(N)
        hawk_payoffs = generate_hawk_payoffs(N, cost)

        gradients[popsizeId].append([])
        for percent in range(100):  # for each percent
            k = (percent * Z) / 100
            gradients[popsizeId][costId].append(gradientOfSelection(k, Z, b, N, dove_payoffs, hawk_payoffs))
print(gradients)


for popsizeId in range(len(population_sizes)):
    for costId in range(len(fight_costs)):
        cost = fight_costs[costId]
        popSize = population_sizes[popsizeId]
        plt.plot(gradients[popsizeId][costId], label="cost=" + str(cost) + ", Z=" + str(popSize))

plt.legend()
plt.xlabel('percentage of doves in the population')
plt.ylabel('gradient of selection')
plt.title('Gradient of selection and equilibria of the N-person Hawk-Dove Game in finite populations')
plt.show()

import math
import scipy.special
import numpy as np
import matplotlib.pyplot as plt


def generate_dove_payoffs(N):
    """ Initializes the payoffs array for doves in all configurations of k and N """
    dove_payoffs = []
    for k in range(0, N + 1):
        dove_payoffs.append(dove_payoff(N, k))
    return dove_payoffs


def generate_hawk_payoffs(N, fight_cost):
    """ Initializes the payoffs array for hawks in all configurations of k and N """
    hawk_payoffs = []
    for k in range(0, N + 1):
        hawk_payoffs.append(hawk_payoff(N, k, fight_cost))
    return hawk_payoffs


def dove_payoff(N, k):
    """ returns the payoff for a dove in a sample of size N containing k doves """
    nb_hawk = N - k
    R = 1  # R (reward) is assumed to be 1
    if nb_hawk == 0:
        return R / N
    else:
        return 0


def hawk_payoff(N, k, Ch):
    """ returns the payoff for a hawk in a sample of size N containing k doves """
    nb_hawk = N - k
    if nb_hawk == 0:
        return 0
    R = 1  # R (reward) is assumed to be 1
    value = (R - ((nb_hawk - 1) * Ch)) / nb_hawk
    return value


def averageFitnessDove(k, Z, N, dove_payoffs):
    """ returns the average fitness of all the doves in the population of size Z with k doves and sample size N """
    sumAvg = []
    for i in range(N):
        val1 = scipy.special.binom(k - 1, i)
        val2 = scipy.special.binom(Z - k, N - i - 1)
        denom = scipy.special.binom(Z - 1, N - 1)
        avg = (val1 * val2) / denom
        sumAvg.append(avg * dove_payoffs[i + 1])
    return np.sum(sumAvg)


def averageFitnessHawk(k, Z, N, hawk_payoffs):
    """ returns the average fitness of all the hawks in the population of size Z with k doves and sample size N """
    sumAvg = []
    for i in range(N):
        val1 = scipy.special.binom(k, i)
        val2 = scipy.special.binom(Z - k - 1, N - i - 1)
        denom = scipy.special.binom(Z - 1, N - 1)
        avg = (val1 * val2) / denom
        sumAvg.append(avg * hawk_payoffs[i])
        # sumAvg.append(avg * hawk_payoff(N, k, ))
    return np.sum(sumAvg)


def probDoveChangeToHawk(b, k, Z, N, dove_payoffs, hawk_payoffs):
    """ Fermi-like rule: returns the probability that a dove changes to a hawk """
    avgFitnessDoves = averageFitnessDove(k, Z, N, dove_payoffs)
    avgFitnessHawks = averageFitnessHawk(k, Z, N, hawk_payoffs)
    return 1. / (1. + np.exp(-b * (avgFitnessDoves - avgFitnessHawks)))


def probHawkChangeToDove(b, k, Z, N, dove_payoffs, hawk_payoffs):
    """ Fermi-like rule: returns the probability that a hawk changes to a dove """
    avgFitnessDoves = averageFitnessDove(k, Z, N, dove_payoffs)
    avgFitnessHawks = averageFitnessHawk(k, Z, N, hawk_payoffs)
    return 1. / (1. + np.exp(-b * (avgFitnessHawks - avgFitnessDoves)))


def probPopGainOneDove(k, Z, N, b, dove_payoffs, hawk_payoffs):
    first = k / Z
    second = (Z - k) / Z
    fitnessDiff = averageFitnessDove(k, Z, N, dove_payoffs) - averageFitnessHawk(k, Z, N, hawk_payoffs)
    third = 1.0 / 1 + np.exp(-b * fitnessDiff)
    return first * second * third


def probPopLoseOneDove(k, Z, N, b, dove_payoffs, hawk_payoffs):
    first = k / Z
    second = (Z - k) / Z
    fitnessDiff = averageFitnessDove(k, Z, N, dove_payoffs) - averageFitnessHawk(k, Z, N, hawk_payoffs)
    third = 1.0 / 1 + np.exp(+b * fitnessDiff)
    return first * second * third


def gradientOfSelection(k, Z, b, N, dove_payoffs, hawk_payoffs):
    first = k / Z
    second = (Z - k) / Z
    fitnessDiff = averageFitnessDove(k, Z, N, dove_payoffs) - averageFitnessHawk(k, Z, N, hawk_payoffs)
    third = b / 2 * fitnessDiff
    return first * second * math.tanh(third)


vals = []

Z = 100
Nvals = [5, 10, 20, 50, 100]
for nId in range(len(Nvals)):  # slow but meh, works ...
    N = Nvals[nId]
    print(N)
    vals.append([])
    for percent in range(100):
        k = (percent / 100) * Z

        found_equi = False
        cost = 0.0
        dove_payoffs = generate_dove_payoffs(N)
        while not found_equi:
            hawk_payoffs = generate_hawk_payoffs(N, cost)
            gradient = gradientOfSelection(k, Z, 1, N, dove_payoffs, hawk_payoffs)
            if gradient >= 0 or math.isnan(gradient) or cost > 2:
                vals[nId].append(cost)
                found_equi = True
            else:
                cost += 0.001

for nId in range(len(Nvals)):
    plt.plot(vals[nId], [x for x in range(100)], label='N=' + str(Nvals[nId]))
plt.xlim(0, 1)
plt.legend()
plt.xlabel('cost for hawks fighting')
plt.ylabel('fraction of doves at equilibrium')
plt.title('Equilibria of the N-person Hawk-Dove Game in finite populations for different values of cost cH,'
          ' and a population size of Z=100')
plt.show()
